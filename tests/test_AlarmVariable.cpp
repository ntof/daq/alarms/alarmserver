/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-18T10:59:26+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <future>
#include <string>
#include <thread>

#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "AlarmRegistry.h"
#include "AlarmServer.h"
#include "AlarmVariable.h"
#include "test_helpers.hpp"

using namespace ntof::dim;
using namespace ntof::alarm;
using namespace ntof;
using namespace std::chrono;

typedef std::shared_ptr<AlarmVariable> Shared;

class TestAlarmVariable : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAlarmVariable);
    CPPUNIT_TEST(test_alarmdb);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override {}

    void tearDown() override
    {
        AlarmRegistry::destroy();
        server::AlarmServer::destroy();
    }

    void test_alarmdb()
    {
        AlarmRegistry &registry(AlarmRegistry::instance());
        /* looping get and create in parallel */
        std::future<bool> ret = std::async(std::launch::deferred, [&registry]() {
            time_point<steady_clock> now = steady_clock::now();

            Shared alarm;
            while (!alarm && (steady_clock::now() < now + milliseconds(500)))
            {
                alarm = registry.getAlarm("1000");
            }
            return bool(alarm);
        });

        std::this_thread::sleep_for(milliseconds(10));
        for (std::size_t i = 0; i <= 1000; ++i)
        {
            /* create new alarms */
            Shared alarm = registry.buildAlarm(
                "Message_" + std::to_string(i), AlarmVariable::CRITICAL,
                std::to_string(i), "system", "subsystem", "source",
                "experience");
            alarm->setActive(true);
        }

        ret.wait();
        EQ(true, ret.get());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestAlarmVariable);
