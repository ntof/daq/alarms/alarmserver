//
// Created by matteof on 9/30/20.
//

#include <boost/filesystem.hpp>

#include <DIMAck.h>
#include <DIMException.h>
#include <DIMSuperClient.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <pugixml.hpp>

#include "AlarmRegistry.h"
#include "AlarmServer.h"
#include "Config.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::dim;
using namespace ntof::alarm;
using namespace ntof;

typedef std::shared_ptr<AlarmVariable> Shared;

class TestAlarmServer : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestAlarmServer);
    CPPUNIT_TEST(test_service);
    CPPUNIT_TEST_SUITE_END();

protected:
    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<ntof::dim::DIMSuperClient> m_cmd;
    bfs::path dataDir_;

public:
    void setUp() override
    {
        dataDir_ = bfs::path(SRCDIR) / "tests" / "data";
        m_dim.reset(new DimTestHelper());
        AlarmRegistry::destroy();
        server::AlarmServer::destroy();
        AlarmRegistry::instance();
        server::AlarmServer::instance();
        m_cmd.reset(
            new ntof::dim::DIMSuperClient(Config::instance().getServiceName()));
    }

    void tearDown() override
    {
        AlarmRegistry::instance().clearAllAlarms();
        AlarmRegistry::destroy();
        server::AlarmServer::destroy();
        m_cmd.reset();
        m_dim.reset();
    }

    static Shared createAlarm(const std::string &ident,
                              const AlarmVariable::Severity &severity)
    {
        Shared alarm = AlarmRegistry::instance().buildAlarm(
            "Message_" + ident, severity, ident, "system", "subsystem",
            "source", "experience");
        alarm->setActive(true);
        return alarm;
    }

    DIMAck sendCommand(const std::string &cmd, const std::string &ident)
    {
        pugi::xml_document doc;
        pugi::xml_node cmdNode = doc.append_child("command");
        cmdNode.append_attribute("name").set_value(cmd.c_str());
        cmdNode.append_attribute("ident").set_value(ident.c_str());
        return m_cmd->sendCommand(doc);
    }

    void test_service()
    {
        //  DIMAck ack = sendCommand("wrongCommand", "ident");
        // EQ(DIMAck::Status::Rejected, ack.getStatus());
        CPPUNIT_ASSERT_THROW(sendCommand("wrongCommand", "ident"), DIMException);

        // Add a fake alarms
        Shared alarm1 = createAlarm("ident1", AlarmVariable::Severity::CRITICAL);
        Shared alarm2 = createAlarm("ident2", AlarmVariable::Severity::ERROR);

        // Wait to be published
        XMLServiceCheck serviceCheck;
        EQ(true,
           serviceCheck.check(
               Config::instance().getServiceName(),
               [&alarm1, &alarm2](pugi::xml_document &doc) {
                   pugi::xml_node serverNode = doc.child("alarmserver");
                   pugi::xml_node alarmsNode = serverNode.child("alarms");
                   // Alarms are sorted by date
                   pugi::xml_node alarm1Node = alarmsNode.last_child();
                   pugi::xml_node alarm2Node = alarmsNode.first_child();

                   return alarm1->getIdentifier() ==
                       alarm1Node.attribute("ident").as_string() &&
                       alarm2->getIdentifier() ==
                       alarm2Node.attribute("ident").as_string() &&
                       std::string("false") ==
                       alarm1Node.attribute("masked").as_string() &&
                       std::string("false") ==
                       alarm1Node.attribute("disabled").as_string() &&
                       std::string("false") ==
                       alarm2Node.attribute("masked").as_string() &&
                       std::string("false") ==
                       alarm2Node.attribute("disabled").as_string();
               }));

        DIMAck ack = sendCommand("mask", "ident1");
        EQ(DIMAck::Status::OK, ack.getStatus());

        DIMAck ack2 = sendCommand("disable", "ident2");
        EQ(DIMAck::Status::OK, ack2.getStatus());

        // Wait check they are now masked and disabled
        EQ(true,
           serviceCheck.check(
               Config::instance().getServiceName(),
               [&alarm1, &alarm2](pugi::xml_document &doc) {
                   pugi::xml_node serverNode = doc.child("alarmserver");
                   pugi::xml_node alarmsNode = serverNode.child("alarms");
                   pugi::xml_node alarm1Node = alarmsNode.last_child();
                   pugi::xml_node alarm2Node = alarmsNode.first_child();
                   return alarm1->getIdentifier() ==
                       alarm1Node.attribute("ident").as_string() &&
                       alarm2->getIdentifier() ==
                       alarm2Node.attribute("ident").as_string() &&
                       std::string("true") ==
                       alarm1Node.attribute("masked").as_string() &&
                       std::string("false") ==
                       alarm1Node.attribute("disabled").as_string() &&
                       std::string("false") ==
                       alarm2Node.attribute("masked").as_string() &&
                       std::string("true") ==
                       alarm2Node.attribute("disabled").as_string();
               }));

        createAlarm("ident3", AlarmVariable::Severity::WARNING);
        createAlarm("ident4", AlarmVariable::Severity::INFO);
        createAlarm("ident5", AlarmVariable::Severity::ERROR);
        createAlarm("ident6", AlarmVariable::Severity::ERROR);

        serviceCheck.check(
            Config::instance().getServiceName(), [](pugi::xml_document &doc) {
                pugi::xml_node serverNode = doc.child("alarmserver");
                pugi::xml_node statsNode = serverNode.child("stats");
                return statsNode.attribute("critical").as_uint() == 1 &&
                    statsNode.attribute("error").as_uint() == 3 &&
                    statsNode.attribute("warning").as_uint() == 1 &&
                    statsNode.attribute("info").as_uint() == 1 &&
                    statsNode.attribute("none").as_uint() == 0 &&
                    statsNode.attribute("masked").as_uint() == 1 &&
                    statsNode.attribute("disabled").as_uint() == 1;
            });
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestAlarmServer);
