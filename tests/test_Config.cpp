//
// Created by matteof on 9/30/20.
//

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <pugixml.hpp>

#include "Config.hpp"
#include "NTOFException.h"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::alarm;
using namespace ntof;

class TestConfig : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfig);
    CPPUNIT_TEST(test_Config_Exceptions);
    CPPUNIT_TEST(test_FullConfig);
    CPPUNIT_TEST_SUITE_END();

protected:
    bfs::path dataDir_;

public:
    void setUp() override { dataDir_ = bfs::path(SRCDIR) / "tests" / "data"; }

    void tearDown() override
    {
        Config::load((dataDir_ / "alarms_unittest.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
    }

    void test_Config_Exceptions()
    {
        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "no_existing_file.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "alarms_unittest_no_zone.xml").string(),
                         (dataDir_ / "no_existing_file.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load(
                (dataDir_ / "alarms_unittest_no_providers.xml").string(),
                (dataDir_ / "no_existing_file.xml").string()),
            NTOFException);
    }

    void test_FullConfig()
    {
        Config &conf = Config::instance();
        EQ(std::string("ntof-alarms"), conf.getDIMServerName());
        EQ(std::string("Alarms"), conf.getServiceName());
        EQ(std::string("EAR0"), conf.getExperiment());
        EQ(std::string("/tmp/Alarms"), conf.getPersistencyFilePath());

        std::shared_ptr<pugi::xml_document> providersDoc = conf.getProvidersDoc();
        EQ(true, providersDoc != nullptr);
        auto providerNodes = providersDoc->first_child().children("provider");
        size_t nProvider = std::distance(providerNodes.begin(),
                                         providerNodes.end());
        EQ((size_t) 4, nProvider);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfig);
