include(Tools)

if(NOT BOOST_ROOT AND IS_DIRECTORY "/opt/ntof/boost")
    message(STATUS "Using preferred boost version: /opt/ntof/boost")
    set(BOOST_ROOT "/opt/ntof/boost" CACHE PATH "")
endif()

find_package(Boost REQUIRED system filesystem program_options thread serialization)

find_library(PTHREAD_LIBRARIES pthread)
if(PTHREAD_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${PTHREAD_LIBRARIES})
endif()

# for interprocess
find_library(RT_LIBRARIES rt)
if(RT_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${RT_LIBRARIES})
endif()
