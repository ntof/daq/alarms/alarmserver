/*
 * AlarmProviderFactory.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef ALARMPROVIDERFACTORY_H_
#define ALARMPROVIDERFACTORY_H_

#include <memory>

#include "providers/AlarmProvider.h"
#include "pugixml.hpp"

namespace ntof {
namespace alarm {

typedef std::shared_ptr<AlarmProvider> ProviderPtr;

/**
 * Build alarm providers
 */
class AlarmProviderFactory
{
public:
    static ProviderPtr buildProvider(pugi::xml_node &providerNode);

private:
    AlarmProviderFactory() = default;
    virtual ~AlarmProviderFactory() = default;
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* ALARMPROVIDERFACTORY_H_ */
