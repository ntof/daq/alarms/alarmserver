/*
 * AlarmServer.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef INCLUDE_ALARMSERVER_H_
#define INCLUDE_ALARMSERVER_H_

#include <string>

#include <pugixml.hpp>

extern bool signalReceived;

namespace ntof {
namespace alarm {

class AlarmRunner
{
public:
    AlarmRunner(std::string iConf, std::string iMiscConf);

    /**
     * Infinite loop that manage all the alarms
     */
    [[noreturn]] void runner();

protected:
    const std::string m_configFile;
    const std::string m_configMiscFile;
};

} // namespace alarm
} // namespace ntof

#endif /* INCLUDE_ALARMSERVER_H_ */
