/*
 * XMLMessage.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: mdonze
 */

#include "XMLMessage.h"

#include <sstream>

namespace ntof {
namespace alarm {
namespace server {

/**
 * Build an empty message
 */
XMLMessage::XMLMessage() : header("0\n") {}

XMLMessage::XMLMessage(std::string &xmlContent)
{
    std::ostringstream oss;
    oss << xmlContent.length() << '\n';
    header = oss.str();
    body = xmlContent;
}

const std::string &XMLMessage::getHeader()
{
    return header;
}

const std::string &XMLMessage::getBody()
{
    return body;
}
} // namespace server
} // namespace alarm
} // namespace ntof
