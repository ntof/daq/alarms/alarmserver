/*
 * XMLMessage.h
 *
 *  Created on: Nov 27, 2014
 *      Author: mdonze
 */

#ifndef XMLMESSAGE_H_
#define XMLMESSAGE_H_

#include <memory>
#include <string>

namespace ntof {
namespace alarm {
namespace server {
/**
 * Represents a message
 */
class XMLMessage
{
public:
    typedef std::shared_ptr<XMLMessage> Shared;

    XMLMessage();
    explicit XMLMessage(std::string &xmlContent);
    virtual ~XMLMessage() = default;
    const std::string &getHeader();
    const std::string &getBody();

private:
    std::string header;
    std::string body;
};
} // namespace server
} // namespace alarm
} // namespace ntof

#endif /* XMLMESSAGE_H_ */
