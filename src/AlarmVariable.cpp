/*
 * AlarmVariable.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: mdonze
 */

#include "AlarmVariable.h"

#include <algorithm>
#include <cctype>
#include <chrono>
#include <iostream>
#include <string>

#include <NTOFLogging.hpp>

#include "AlarmPersistency.h"
#include "AlarmRegistry.h"

namespace {
int64_t getTimeMs()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::system_clock::now().time_since_epoch())
        .count();
}
} // namespace

namespace ntof {
namespace alarm {

// C++11 problem with constexpr static. This can be removed when migrating to
// C++17
constexpr std::initializer_list<AlarmVariable::Severity>
    AlarmVariable::AllSeverity;

AlarmVariable::AlarmVariable() :
    active_(false),
    masked_(false),
    disabled_(false),
    severity_(NONE),
    lastActive(0),
    firstUse(false)
{}

AlarmVariable::AlarmVariable(const std::string &message,
                             Severity severity,
                             const std::string &identifier,
                             const std::string &systemName,
                             const std::string &subSystem,
                             const std::string &source,
                             const std::string &experience) :
    message_(message),
    active_(false),
    masked_(false),
    disabled_(false),
    severity_(severity),
    identifier_(escapeIdent(identifier)),
    systemName_(systemName),
    subSystemName_(subSystem),
    alarmSource_(source),
    experience_(experience),
    lastActive(0),
    firstUse(true)
{
    if (!message.empty())
    {
        message_[0] = std::toupper(message_[0]);
    }
    AlarmPersistency::instance().restoreVariableState(this);
}

AlarmVariable &AlarmVariable::operator=(AlarmVariable &&other)
{
    std::lock_guard<std::mutex> lock(lock_);
    message_ = other.message_;
    active_ = other.active_;
    masked_ = other.masked_;
    disabled_ = other.disabled_;
    const_cast<Severity &>(severity_) = other.severity_;
    const_cast<std::string &>(identifier_) = other.identifier_;
    const_cast<std::string &>(systemName_) = other.systemName_;
    const_cast<std::string &>(subSystemName_) = other.subSystemName_;
    const_cast<std::string &>(alarmSource_) = other.alarmSource_;
    const_cast<std::string &>(experience_) = other.experience_;
    lastActive = other.lastActive;
    firstUse = other.firstUse;
    return *this;
}

std::string AlarmVariable::getMessage() const
{
    std::lock_guard<std::mutex> lock(lock_);
    if (suffix_.empty())
    {
        return message_;
    }
    else
    {
        return message_ + " " + suffix_;
    }
}

void AlarmVariable::setSuffix(const std::string &msg)
{
    bool changed;
    {
        std::lock_guard<std::mutex> lock(lock_);
        changed = (suffix_ != msg);
        suffix_ = msg;
    }
    if (changed)
    {
        AlarmRegistry::instance().postNotification(shared());
    }
}

bool AlarmVariable::setActive(bool active)
{
    {
        std::lock_guard<std::mutex> lock(lock_);
        if ((active_ == active) && !firstUse)
        {
            // Alarm not changed
            return false;
        }
        if (!active && masked_ && !firstUse)
        {
            masked_ = false;
        }

        if (firstUse)
        {
            firstUse = false;
        }

        if (active && (active_ != active))
        {
            lastActive = getTimeMs();
        }
        active_ = active;
    }
    AlarmRegistry::instance().postNotification(shared());
    return true;
}

bool AlarmVariable::isActive() const
{
    std::lock_guard<std::mutex> lock(lock_);
    return active_;
}

void AlarmVariable::setMasked(bool masked)
{
    std::lock_guard<std::mutex> lock(lock_);
    masked_ = masked;
}

bool AlarmVariable::isMasked() const
{
    std::lock_guard<std::mutex> lock(lock_);
    return masked_;
}

void AlarmVariable::setDisabled(bool disabled)
{
    std::lock_guard<std::mutex> lock(lock_);
    disabled_ = disabled;
}

bool AlarmVariable::isDisabled() const
{
    std::lock_guard<std::mutex> lock(lock_);
    return disabled_;
}

int64_t AlarmVariable::getWhen() const
{
    std::lock_guard<std::mutex> lock(lock_);
    return lastActive;
}

void AlarmVariable::setWhen(int64_t ts)
{
    std::lock_guard<std::mutex> lock(lock_);
    lastActive = ts;
}

std::ostream &operator<<(std::ostream &os, const AlarmVariable &var)
{
    os << var.getIdentifier() << ' ' << var.isActive() << ' ' << var.getWhen()
       << ' ' << var.isMasked() << ' ' << var.isDisabled() << "\n";
    return os;
}

std::istream &operator>>(std::istream &is, AlarmVariable &var)
{
    std::lock_guard<std::mutex> lock(var.lock_);
    is >> const_cast<std::string &>(var.identifier_) >> var.active_ >>
        var.lastActive >> var.masked_ >> var.disabled_;
    return is;
}

/**
 * Convert the severity string to a Severity enumeration
 * @param sev
 * @return
 */
AlarmVariable::Severity AlarmVariable::severityFromString(const std::string &sev)
{
    std::string severity(sev);
    // To upper case
    std::transform(severity.begin(), severity.end(), severity.begin(),
                   ::toupper);

    if (severity == "INFO")
    {
        return INFO;
    }
    else if (severity == "WARNING")
    {
        return WARNING;
    }
    else if (severity == "ERROR")
    {
        return ERROR;
    }
    else if (severity == "CRITICAL")
    {
        return CRITICAL;
    }
    else
    {
        return NONE;
    }
}

/**
 * Convert severity to a nice displayabled string
 * @param sev
 * @return
 */
std::string AlarmVariable::severityToString(AlarmVariable::Severity sev)
{
    switch (sev)
    {
    case INFO: return "INFO";
    case WARNING: return "WARNING";
    case ERROR: return "ERROR";
    case CRITICAL: return "CRITICAL";
    default: return "NONE";
    }
}

std::string AlarmVariable::escapeIdent(const std::string &ident)
{
    std::ostringstream ret;
    bool wasSpace = false;
    for (const char it : ident)
    {
        if (it == ' ')
        {
            wasSpace = true;
        }
        else
        {
            if (wasSpace)
            {
                ret << (char) std::toupper(it);
                wasSpace = false;
            }
            else
            {
                ret << it;
            }
        }
    }
    // TODO: UrlEncode?
    return ret.str();
}
} // namespace alarm
} // namespace ntof
