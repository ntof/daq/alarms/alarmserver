/*
 * AlarmVariable.h
 *
 *  Created on: Nov 20, 2014
 *      Author: mdonze
 */

#ifndef ALARMVARIABLE_H_
#define ALARMVARIABLE_H_

#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

#define MAX_ALARMVAR_SIZE 8192

namespace ntof {
namespace alarm {
class AlarmPersistency;
class AlarmRegistry;
/**
 * Represents an alarm value
 */
class AlarmVariable
{
public:
    typedef std::shared_ptr<AlarmVariable> Shared;
    typedef std::unordered_map<std::string, AlarmVariable::Shared> AlarmMap;
    enum Severity
    {
        NONE = 0,
        INFO,
        WARNING,
        ERROR,
        CRITICAL
    };

    static constexpr std::initializer_list<Severity> AllSeverity = {
        NONE, INFO, WARNING, ERROR, CRITICAL};

    AlarmVariable();
    virtual ~AlarmVariable() = default;

    std::string getMessage() const;
    void setSuffix(const std::string &msg);
    bool setActive(bool active);
    bool isActive() const;

    void setMasked(bool masked);
    bool isMasked() const;
    void setDisabled(bool disabled);
    bool isDisabled() const;

    int64_t getWhen() const;
    void setWhen(int64_t ts);

    inline bool isValid() const { return !identifier_.empty(); }
    inline const std::string &getIdentifier() const { return identifier_; }
    inline Severity getSeverity() const { return severity_; }
    inline const std::string &getSystemName() const { return systemName_; }
    inline const std::string &getSubSystemName() const
    {
        return subSystemName_;
    }
    inline const std::string &getExperience() const { return experience_; }
    inline const std::string &getAlarmSource() const { return alarmSource_; }

    /**
     * Convert the severity string to a Severity enumeration
     * @param sev
     * @return
     */
    static Severity severityFromString(const std::string &sev);

    /**
     * Convert severity to a nice displayabled string
     * @param sev
     * @return
     */
    static std::string severityToString(Severity sev);

    /**
     * @brief get shared object
     * @details only valid for alarms built by AlarmRegistry
     */
    inline Shared shared() const { return m_weak.lock(); }

protected:
    friend class AlarmPersistency;
    friend class AlarmRegistry;
    friend std::istream &operator>>(std::istream &is, AlarmVariable &var);

    const Severity severity_;         //!<< Alarm severity
    const std::string identifier_;    //!<< Alarm unique identifier
    const std::string systemName_;    //!<< Alarm system source name
    const std::string subSystemName_; //!<< Alarm sub-system source name
    const std::string alarmSource_;   //!<< Alarm source
    const std::string experience_;    //!<< Alarm _TOF experience

    std::string message_; //!<< Alarm message
    std::string suffix_;  //!<< Message suffix
    bool active_;         //!<< Alarm active?
    bool masked_;         //!<< Alarm masked?
    bool disabled_;       //!<< Alarm disabled?
    int64_t lastActive;   //!<< Timestamp when the alarm was active last
    bool firstUse;        //!<< First use of this object?

    mutable std::mutex lock_;
    std::weak_ptr<AlarmVariable> m_weak;

    AlarmVariable(const AlarmVariable &) = delete; //!<< Copy not allowed
    AlarmVariable &operator=(const AlarmVariable &) = delete; //!<< Copy not
                                                              //!< allowed
    AlarmVariable(AlarmVariable &&) = default;
    AlarmVariable &operator=(AlarmVariable &&);

    AlarmVariable(const std::string &message,
                  Severity severity,
                  const std::string &identifier,
                  const std::string &systemName,
                  const std::string &subSystem,
                  const std::string &source,
                  const std::string &experience);

    static std::string escapeIdent(const std::string &ident);
};

std::ostream &operator<<(std::ostream &os,
                         const ntof::alarm::AlarmVariable &var);
std::istream &operator>>(std::istream &is, ntof::alarm::AlarmVariable &var);

} // namespace alarm
} // namespace ntof

#endif /* ALARMVARIABLE_H_ */
