/*
 * AlarmProviderFactory.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#include "AlarmProviderFactory.h"

#include <string>

#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "providers/DIMDataSetProvider.h"
#include "providers/DIMStateProvider.h"
#include "providers/DIMonProvider.h"

namespace ntof {
namespace alarm {

/**
 * Build an alarm provider depending on configuration XML node
 * @param providerNode
 * @return
 */
ProviderPtr AlarmProviderFactory::buildProvider(pugi::xml_node &providerNode)
{
    std::string type = providerNode.attribute("type").as_string("undefined");

    // To upper case
    std::transform(type.begin(), type.end(), type.begin(), ::toupper);

    switch (fromString(type))
    {
    case ProviderType::DIMON:
        return ProviderPtr(new DIMonProvider(providerNode));
    case ProviderType::DIMSTATE:
        return ProviderPtr(new DIMStateProvider(providerNode));
    case ProviderType::DIMDATASET:
        return ProviderPtr(new DIMDataSetProvider(providerNode));
    default: return ProviderPtr(nullptr);
    }
}

} /* namespace alarm */
} /* namespace ntof */
