/*
 * DIMonProvider.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef DIMONPROVIDER_H_
#define DIMONPROVIDER_H_

#include <memory>
#include <vector>

#include <providers/AlarmProvider.h>

namespace ntof {
namespace alarm {
class DIMonServer;

/**
 * Provide alarms for monitoring DIM services
 */
class DIMonProvider : public AlarmProvider
{
public:
    typedef std::shared_ptr<DIMonServer> ServerPtr;
    typedef std::vector<ServerPtr> ServerVector;

    explicit DIMonProvider(pugi::xml_node &providerNode);
    ~DIMonProvider() override;

private:
    ServerVector servers;
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* DIMONPROVIDER_H_ */
