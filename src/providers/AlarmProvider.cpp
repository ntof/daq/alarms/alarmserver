/*
 * AlarmProvider.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#include "AlarmProvider.h"

#include <iostream>

namespace ntof {
namespace alarm {

AlarmProvider::AlarmProvider(pugi::xml_node &providerNode) :
    name_(providerNode.attribute("type").as_string("UNKNOWN"))
{}

/**
 * Gets the name of this provider
 * @return
 */
std::string AlarmProvider::getName()
{
    return name_;
}

} /* namespace alarm */
} /* namespace ntof */
