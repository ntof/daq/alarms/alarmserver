/*
 * DIMonServer.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#include "DIMonServer.h"

#include <iostream>
#include <string>

#include <NTOFException.h>
#include <NTOFLogging.hpp>

#include "AlarmRegistry.h"
#include "AlarmRunner.h"
#include "AlarmVariable.h"
#include "Config.hpp"
#include "DIMonProvider.h"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::alarm::DIMonServer::ServiceReader>;

namespace ntof {
namespace alarm {

DIMonServer::DIMonServer(DIMonProvider &provider, pugi::xml_node &serverNode) :
    srvName_(serverNode.attribute("name").as_string()), srvInfo(nullptr)
{
    std::string display = serverNode.attribute("display").as_string();
    std::string system = serverNode.attribute("system").as_string(
        display.c_str());
    std::string noLink = "No link on " + display;
    std::string ident = "DIMON/" + srvName_ + "#SERVER";
    std::string expName = Config::instance().getExperiment();
    noLinkAlarm_ = AlarmRegistry::instance().buildAlarm(
        noLink, AlarmVariable::ERROR, ident, system, srvName_,
        provider.getName(), expName);
    std::string dnsService = (srvName_ + "/SERVICE_LIST");
    for (pugi::xml_node service = serverNode.first_child(); service;
         service = service.next_sibling())
    {
        // Check if it's a DIM service to be monitored
        if (std::string(service.name()) == "service")
        {
            std::string svcName = service.attribute("name").as_string();
            std::string severity =
                service.attribute("severity").as_string("ERROR");
            std::string svcIdent = "DIMON/" + srvName_ + "#" + svcName;
            std::ostringstream oss;
            oss << "Service " << svcName << " is missing on server " << display
                << "!";
            std::string svcDisplay(oss.str());
            AlarmPtr alarm = AlarmRegistry::instance().buildAlarm(
                svcDisplay, AlarmVariable::severityFromString(severity),
                svcIdent, system, svcName, provider.getName(), expName);
            alarms_[svcName] = alarm;
        }
    }
    // Connect to the SERVICE_LIST service to monitor this service
    srvInfo = new DimInfo(dnsService.c_str(), (void *) nullptr, 0, this);
}

DIMonServer::~DIMonServer()
{
    delete srvInfo;
    srvInfo = nullptr;
}

void DIMonServer::infoHandler()
{
    if (srvInfo->getSize() == 0)
    {
        // NO LINK on server
        noLinkAlarm_->setActive(true);
        // Remove services errors
        for (const AlarmsMap::value_type &it : alarms_)
        {
            if (it.second->isActive())
            {
                it.second->setActive(false);
            }
        }
    }
    else
    {
        // Link ok, we will refresh the list
        // We can't do it here as we are already into DIM thread (it will cause
        // deadlocks) That's why the ServiceReader exists
        ServiceReader::instance().requestRead(this);
    }
}

/**
 * Gets the DIM server name
 * @return
 */
std::string DIMonServer::getServerName()
{
    return srvName_;
}

/**
 * Called by the reader thread
 * @param services
 */
void DIMonServer::updateSvcList(const std::string &svcContent)
{
    std::set<std::string> services;

    // Parse result from DIM server
    std::istringstream input;
    input.str(svcContent);
    for (std::string line; std::getline(input, line);)
    {
        size_t pos = line.find_first_of('|');
        if (pos == std::string::npos)
        {
            pos = line.length();
        }
        std::string svcName = line.substr(0, pos);
        services.insert(svcName);
    }

    noLinkAlarm_->setActive(false);

    // Go trough actual services to get services back
    for (const std::string &service : services)
    {
        auto alIt = alarms_.find(service);
        if (alIt != alarms_.end())
        {
            // Found, service is here
            alIt->second->setActive(false);
        }
    }

    // Gets services not here
    for (AlarmsMap::value_type &it : alarms_)
    {
        auto newIt = services.find(it.first);
        if (newIt == services.end())
        {
            // Not found, service is not here
            it.second->setActive(true);
        }
    }
}

/**
 * Do cleanup of pending requests
 */
void DIMonServer::cleanup()
{
    ServiceReader::instance().cleanup();
}

DIMonServer::ServiceReader::ServiceReader() :
    Thread("DIMonSrvRequest"), m_queue("DIMonSrvRequest", 2000)
{
    start();
}

void DIMonServer::ServiceReader::thread_func()
{
    while (m_started)
    {
        // Replace queue container by smart-pointer or clear it before
        DIMonServer *request = m_queue.pop();
        std::string svcList = (request->getServerName() + "/SERVICE_LIST");
        // Query actual service list
        DimCurrentInfo info(svcList.c_str(), (char *) "");
        std::string svcListData = info.getString();
        // Call caller updateService function
        request->updateSvcList(svcListData);
    }
}

/**
 * Request service list readout
 * @param server
 */
void DIMonServer::ServiceReader::requestRead(DIMonServer *server)
{
    try
    {
        m_queue.post(server);
    }
    catch (const ntof::NTOFException &e)
    {
        LOG(ERROR) << "DIMon reader queue is full. Notification missed..."
                   << std::endl;
    }
}

/**
 * Empty the queue
 */
void DIMonServer::ServiceReader::cleanup()
{
    m_queue.clear();
}

} /* namespace alarm */
} /* namespace ntof */
