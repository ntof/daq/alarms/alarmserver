/*
 * DIMDataSetProvider.h
 *
 *  Created on: Nov 9, 2015
 *      Author: mdonze
 */

#ifndef DIMDATASETPROVIDER_H_
#define DIMDATASETPROVIDER_H_

#include <map>
#include <mutex>
#include <set>
#include <thread>

#include <AlarmVariable.h>
#include <DIMDataSetClient.h>
#include <providers/AlarmProvider.h>

namespace ntof {
namespace alarm {

class DIMDataSetProvider :
    public AlarmProvider,
    public ntof::dim::DIMDataSetClientHandler
{
public:
    explicit DIMDataSetProvider(pugi::xml_node &providerNode);
    virtual ~DIMDataSetProvider();

private:
    /**
     * Called when the DIM service is refreshed by the server
     * @param dataSet New list of data
     * @param client Client responsible of this callback (can be shared)
     */
    void dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                     const ntof::dim::DIMDataSetClient &client);

    /**
     * Called when an error occured on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMDataSetClient &client);

    typedef struct
    {
        double min;             //!< Minimum field value
        double max;             //!< Maximum field value
        int32_t delay;          //!< Minimum refresh time of the variable
        int32_t minActiveSince; //!< Active time
        int32_t maxActiveSince; //!< Active time
        std::string name;       //!< Data name
        std::string unit;       //!< Data unit
        bool unitSet;           //!< Is the unit set for this field
        AlarmVariable::Shared minAlVar; //!< Associated alarm (minimum)
        AlarmVariable::Shared maxAlVar; //!< Associated alarm (maximum)
    } FIELD_DESC;                       //!< Defines a field

    typedef std::map<std::string, FIELD_DESC> AlarmsMap;

    ntof::dim::DIMDataSetClient *client_;
    int32_t minRefresh_; //!< Minimum refresh time
    int32_t lastRefresh_;

    AlarmVariable::Shared noLinkAlarm_;  //!< Alarm raised when no link
    AlarmVariable::Shared timeoutAlVar_; //!< Associated alarm (refresh
                                         //!< timeout)

    AlarmsMap alarms_;

    // Static objects to get a 1Hz timer
    static std::thread *timerThread;
    static std::set<DIMDataSetProvider *> providers;
    static std::mutex timerLock;
    static bool stopTimer;

    static void timerThreadProcedure();
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* DIMDATASETPROVIDER_H_ */
