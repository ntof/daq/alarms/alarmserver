/*
 * DIMStateProvider.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: mdonze
 */
#include "DIMStateProvider.h"

#include <sstream>

#include <NTOFLogging.hpp>

#include "AlarmRegistry.h"
#include "AlarmRunner.h"
#include "Config.hpp"

namespace ntof {
namespace alarm {

/**
 * Constructor
 * @param providerNode
 */
DIMStateProvider::DIMStateProvider(pugi::xml_node &providerNode) :
    AlarmProvider(providerNode),
    client(nullptr),
    label_(providerNode.attribute("display").as_string())
{
    std::string service = providerNode.attribute("service").as_string();
    system_ = providerNode.attribute("system").as_string();
    std::string noLink = "No link on " + label_;
    std::string ident = "STATE/" + label_ + "#SERVER";
    std::string expName = Config::instance().getExperiment();

    pugi::xml_node excludeNode = providerNode.child("exclude");
    for (pugi::xml_node exclude = excludeNode.first_child(); exclude;
         exclude = exclude.next_sibling())
    {
        std::string name = exclude.name();
        if (name == "error")
        {
            const pugi::xml_attribute &codeAttr = exclude.attribute("code");
            if (codeAttr)
            {
                excludedErrorsCode.insert(codeAttr.as_int(0));
            }

            const pugi::xml_attribute &nameAttr = exclude.attribute("name");
            if (nameAttr)
            {
                excludedErrorsName.insert(nameAttr.as_string(""));
            }
        }
        else if (name == "warning")
        {
            const pugi::xml_attribute &codeAttr = exclude.attribute("code");
            if (codeAttr)
            {
                excludedWarningsCode.insert(codeAttr.as_int(0));
            }

            const pugi::xml_attribute &nameAttr = exclude.attribute("name");
            if (nameAttr)
            {
                excludedWarningsName.insert(nameAttr.as_string(""));
            }
        }
    }

    noLinkAlarm_ = AlarmRegistry::instance().buildAlarm(
        noLink, AlarmVariable::ERROR, ident, system_, label_, getName(),
        expName);
    LOG(INFO) << "Connecting to " << service << std::endl;
    client = new ntof::dim::DIMStateClient(service, this);
}

DIMStateProvider::~DIMStateProvider()
{
    LOG(INFO) << "Destorying DIMStateProvider" << std::endl;
    delete client;
}

/**
 * Called when a DIM state is in error
 * @param errCode
 * @param errMsg
 * @param client
 */
void DIMStateProvider::errorReceived(
    int32_t /* errCode */,
    const std::string & /* errMsg */,
    const ntof::dim::DIMStateClient * /* client */)
{
    buildAndRefreshAlarms();
}

/**
 * Called when a DIM state is refreshed
 * @param errCode
 * @param errMsg
 * @param client
 */
void DIMStateProvider::stateReceived(
    int32_t /* stateValue */,
    const std::string & /* stateString */,
    const ntof::dim::DIMStateClient * /* client */)
{
    buildAndRefreshAlarms();
}

/**
 * No link on DIM state service
 * @param client
 */
void DIMStateProvider::noLink(const ntof::dim::DIMStateClient * /* client */)
{
    // NO LINK on server
    noLinkAlarm_->setActive(true);
    // Remove services errors
    for (AlarmsMap::value_type &it : alarms_)
    {
        it.second->setActive(false);
    }
}

/**
 * Make the list of alarms
 */
void DIMStateProvider::buildAndRefreshAlarms()
{
    ntof::dim::DIMStateClient::ErrWarnVector errors = client->getActiveErrors();
    ntof::dim::DIMStateClient::ErrWarnVector warnings =
        client->getActiveWarnings();

    std::string expName = Config::instance().getExperiment();

    // Build map containing active errors/warning
    AlarmsMap active;
    for (ntof::dim::DIMErrorWarning &it : errors)
    {
        IgnoreCodeSet::iterator exCodeIt = excludedErrorsCode.find(it.getCode());
        IgnoreNameSet::iterator exNameIt = excludedErrorsName.find(
            it.getMessage());
        if ((exCodeIt == excludedErrorsCode.end()) &&
            (exNameIt == excludedErrorsName.end()))
        {
            std::ostringstream oss;
            oss << "STATE/" << label_ << "#ERROR_" << it.getCode();
            std::ostringstream ossMsg;
            ossMsg << "Error #" << it.getCode() << " : ";
            AlarmVariable::Shared al = AlarmRegistry::instance().buildAlarm(
                ossMsg.str(), AlarmVariable::ERROR, oss.str(), system_, label_,
                getName(), expName);
            al->setActive(true);
            al->setSuffix(it.getMessage());
            active[oss.str()] = al;
        }
    }
    for (ntof::dim::DIMErrorWarning &it : warnings)
    {
        IgnoreCodeSet::iterator exCodeIt = excludedWarningsCode.find(
            it.getCode());
        IgnoreNameSet::iterator exNameIt = excludedWarningsName.find(
            it.getMessage());
        if ((exCodeIt == excludedWarningsCode.end()) &&
            (exNameIt == excludedWarningsName.end()))
        {
            std::ostringstream oss;
            oss << "STATE/" << label_ << "#WARNING_" << it.getCode();
            std::ostringstream ossMsg;
            ossMsg << "Error #" << it.getCode() << " : ";
            AlarmVariable::Shared al = AlarmRegistry::instance().buildAlarm(
                ossMsg.str(), AlarmVariable::WARNING, oss.str(), system_,
                label_, getName(), expName);
            al->setActive(true);
            al->setSuffix(it.getMessage());
            active[oss.str()] = al;
        }
    }

    // Clear removed alarms
    for (AlarmsMap::value_type &it : alarms_)
    {
        if (active.find(it.first) == active.end())
        {
            // Not active anymore
            it.second->setActive(false);
        }
    }

    // Add new alarms
    for (AlarmsMap::value_type &it : active)
    {
        AlarmsMap::iterator existIt = alarms_.find(it.first);
        if (existIt == alarms_.end())
        {
            // Is now active
            alarms_[it.first] = it.second;
            it.second->setActive(true);
        }
    }

    // Clear no-link if it was active
    noLinkAlarm_->setActive(false);
}

} // namespace alarm
} // namespace ntof
