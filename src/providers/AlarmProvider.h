/*
 * AlarmProvider.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef ALARMPROVIDER_H_
#define ALARMPROVIDER_H_

#include <string>

#include "pugixml.hpp"

namespace ntof {
namespace alarm {

/**
 * This class is an abstract class for something providing alarms
 */
class AlarmProvider
{
public:
    explicit AlarmProvider(pugi::xml_node &providerNode);
    virtual ~AlarmProvider() = default;

    /**
     * Gets the name of this provider
     * @return
     */
    std::string getName();

private:
    std::string name_; //!<< Provider name
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* ALARMPROVIDER_H_ */
