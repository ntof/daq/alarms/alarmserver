/*
 * DIMonServer.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef DIMONSERVER_H_
#define DIMONSERVER_H_

#include <map>
#include <memory>
#include <set>
#include <string>

#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "pugixml.hpp"

#include <dic.hxx>

namespace ntof {
namespace alarm {

class AlarmVariable;
class DIMonProvider;

/**
 * Represents a server to be monitored by a DIMon provider instance
 */
class DIMonServer : public DimClient
{
public:
    typedef std::shared_ptr<AlarmVariable> AlarmPtr;
    typedef std::map<std::string, AlarmPtr> AlarmsMap;

    DIMonServer(DIMonProvider &provider, pugi::xml_node &serverNode);
    ~DIMonServer() override;

    /**
     * Gets the DIM server name
     * @return
     */
    std::string getServerName();

    /**
     * Do cleanup of pending requests
     */
    static void cleanup();

private:
    void infoHandler() override;
    void updateSvcList(const std::string &svcContent);

    AlarmsMap alarms_;     //!<< Map containing all know services alarms
    AlarmPtr noLinkAlarm_; //!<< No link alarm
    std::string srvName_;  //!<< Server name
    DimInfo *srvInfo;      //!<< DimInfo to connect to the DIM server

    /**
     * This class will read SERVICE_LIST notifications and get actual service
     * list
     */
    class ServiceReader :
        private ntof::utils::Thread,
        public ntof::utils::Singleton<ServiceReader>
    {
    public:
        /**
         * @brief Destructor of the class
         */
        ~ServiceReader() override = default;

        void requestRead(DIMonServer *server);
        void cleanup();

    protected:
        friend class ntof::utils::Singleton<ServiceReader>;

        ServiceReader();

        /**
         * @brief Start the thread
         */
        void thread_func() override;

        ntof::utils::Queue<DIMonServer *> m_queue; //!<< Queue for inter-process
                                                   //!< communication
    };
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* DIMONSERVER_H_ */
