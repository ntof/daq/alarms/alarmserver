/*
 * DIMDataSetProvider.cpp
 *
 *  Created on: Nov 9, 2015
 *      Author: mdonze
 */

#include "DIMDataSetProvider.h"

#include <chrono>
#include <cmath>
#include <mutex>
#include <sstream>
#include <thread>

#include <NTOFLogging.hpp>

#include "AlarmRegistry.h"
#include "AlarmRunner.h"
#include "Config.hpp"

namespace ntof {
namespace alarm {

std::thread *DIMDataSetProvider::timerThread = nullptr;
std::set<DIMDataSetProvider *> DIMDataSetProvider::providers;
std::mutex DIMDataSetProvider::timerLock;
bool DIMDataSetProvider::stopTimer = false;

DIMDataSetProvider::DIMDataSetProvider(pugi::xml_node &providerNode) :
    AlarmProvider(providerNode),
    client_(nullptr),
    minRefresh_(-1),
    lastRefresh_(0)
{
    std::string service = providerNode.attribute("service").as_string();
    std::string system = providerNode.attribute("system").as_string();
    std::string noLink = "Error on " + service + " :";
    std::string ident = "DATASET/" + service + "#SERVER";
    std::string expName = Config::instance().getExperiment();
    noLinkAlarm_ = AlarmRegistry::instance().buildAlarm(
        noLink, AlarmVariable::ERROR, ident, system, service, getName(),
        expName);

    minRefresh_ = providerNode.attribute("refresh").as_int(-1);
    if (minRefresh_ > 0)
    {
        std::string timeOut = "Refresh time-out of service " + service;
        ident = "DATASET/" + service + "#TIMEOUT";
        std::string severity =
            providerNode.attribute("severity").as_string("ERROR");
        timeoutAlVar_ = AlarmRegistry::instance().buildAlarm(
            timeOut, AlarmVariable::severityFromString(severity), ident, system,
            service, getName(), expName);
    }
    else
    {
        minRefresh_ = -1;
    }

    // Gets list of fields to be monitored
    for (pugi::xml_node data = providerNode.first_child(); data;
         data = data.next_sibling())
    {
        FIELD_DESC desc;
        desc.min = data.attribute("min").as_double(nan(""));
        desc.max = data.attribute("max").as_double(nan(""));
        desc.delay = data.attribute("delay").as_int(-1);
        desc.minActiveSince = -1;
        desc.maxActiveSince = -1;
        desc.name = data.attribute("name").as_string();
        pugi::xml_attribute att = data.attribute("unit");
        desc.unit = att.as_string();
        // If unit is not into configuration, don't print thresholds
        if (att)
        {
            desc.unitSet = true;
        }
        else
        {
            desc.unitSet = false;
        }

        std::string field = data.attribute("field").as_string();
        std::string severity = data.attribute("severity").as_string("ERROR");
        if (!std::isnan(desc.min))
        {
            std::string minText = desc.name;
            std::ostringstream oss;
            oss << "DATASET/" << service << "#" << field << "_min";
            ident = oss.str();
            desc.minAlVar = AlarmRegistry::instance().buildAlarm(
                minText, AlarmVariable::severityFromString(severity), ident,
                system, desc.name, getName(), expName);
        }

        if (!std::isnan(desc.max))
        {
            std::string maxText = desc.name;
            std::ostringstream oss;
            oss << "DATASET/" << service << "#" << field << "_max";
            ident = oss.str();
            desc.maxAlVar = AlarmRegistry::instance().buildAlarm(
                maxText, AlarmVariable::severityFromString(severity), ident,
                system, desc.name, getName(), expName);
        }
        alarms_[field] = desc;
    }

    // Connect to DIM dataset
    client_ = new ntof::dim::DIMDataSetClient(service, this);

    // Start the thread
    {
        std::lock_guard<std::mutex> lock(timerLock);
        if (timerThread == nullptr)
        {
            timerThread = new std::thread(
                &DIMDataSetProvider::timerThreadProcedure);
        }
        providers.insert(this);
    }
}

DIMDataSetProvider::~DIMDataSetProvider()
{
    delete client_;
    {
        std::lock_guard<std::mutex> lock(timerLock);
        providers.erase(this);
        if (providers.empty())
        {
            stopTimer = true;
            if (timerThread)
            {
                timerThread->join();
            }
            delete timerThread;
            timerThread = nullptr;
        }
    }
}

/**
 * Called when the DIM service is refreshed by the server
 * @param dataSet New list of data
 * @param client Client responsible of this callback (can be shared)
 */
void DIMDataSetProvider::dataChanged(
    std::vector<ntof::dim::DIMData> &dataSet,
    const ntof::dim::DIMDataSetClient & /* client */)
{
    std::lock_guard<std::mutex> lock(timerLock);
    lastRefresh_ = 0;
    noLinkAlarm_->setActive(false);

    for (ntof::dim::DIMData &it : dataSet)
    {
        auto aIt = alarms_.find(it.getName());
        if (aIt != alarms_.end())
        {
            double value = 0.0;
            switch (it.getDataType())
            {
            case ntof::dim::DIMData::TypeByte:
                value = (double) it.getByteValue();
                break;
            case ntof::dim::DIMData::TypeDouble:
                value = it.getDoubleValue();
                break;
            case ntof::dim::DIMData::TypeFloat:
                value = (double) it.getFloatValue();
                break;
            case ntof::dim::DIMData::TypeInt:
                value = (double) it.getIntValue();
                break;
            case ntof::dim::DIMData::TypeLong:
                value = (double) it.getLongValue();
                break;
            case ntof::dim::DIMData::TypeShort:
                value = (double) it.getShortValue();
                break;
            default:
                LOG(ERROR) << "Unknown data-type for data " << it.getName()
                           << std::endl;
                return;
            }

            if (aIt->second.minAlVar.get() != nullptr)
            {
                if (value < aIt->second.min)
                {
                    if (aIt->second.unitSet)
                    {
                        std::ostringstream oss;
                        oss.precision(3);
                        oss << " below threshold (" << value << aIt->second.unit
                            << " < " << aIt->second.min << aIt->second.unit
                            << ")";
                        aIt->second.minAlVar->setSuffix(oss.str());
                    }
                    else
                    {
                        aIt->second.minAlVar->setSuffix("");
                    }

                    if (aIt->second.delay <= 0)
                    {
                        // Immediate raising of alarm
                        aIt->second.minAlVar->setActive(true);
                    }
                    else
                    {
                        if (aIt->second.minActiveSince == -1)
                        {
                            aIt->second.minActiveSince = 0;
                        }
                    }
                }
                else
                {
                    // Clear error
                    aIt->second.minAlVar->setActive(false);
                    aIt->second.minActiveSince = -1;
                }
            }

            if (aIt->second.maxAlVar.get() != nullptr)
            {
                if (value > aIt->second.max)
                {
                    if (aIt->second.unitSet)
                    {
                        std::ostringstream oss;
                        oss.precision(3);
                        oss << " over threshold (" << value << aIt->second.unit
                            << " > " << aIt->second.max << aIt->second.unit
                            << ")";
                        aIt->second.maxAlVar->setSuffix(oss.str());
                    }
                    else
                    {
                        aIt->second.maxAlVar->setSuffix("");
                    }

                    if (aIt->second.delay <= 0)
                    {
                        // Immediate raising of alarm
                        aIt->second.maxAlVar->setActive(true);
                    }
                    else
                    {
                        if (aIt->second.maxActiveSince == -1)
                        {
                            aIt->second.maxActiveSince = 0;
                        }
                    }
                }
                else
                {
                    // Clear error
                    aIt->second.maxAlVar->setActive(false);
                    aIt->second.maxActiveSince = -1;
                }
            }
        }
    }
}

/**
 * Called when an error occured on client (NO-LINK, NOT READY...)
 * @param errMsg Error message
 * @param client Client responsible for this callback (can be shared)
 */
void DIMDataSetProvider::errorReceived(
    const std::string &errMsg,
    const ntof::dim::DIMDataSetClient & /* client */)
{
    noLinkAlarm_->setActive(true);
    noLinkAlarm_->setSuffix(errMsg);

    for (AlarmsMap::value_type &it : alarms_)
    {
        if (it.second.minAlVar.get())
        {
            it.second.minAlVar->setActive(false);
        }

        if (it.second.maxAlVar.get())
        {
            it.second.maxAlVar->setActive(false);
        }
    }
}

void DIMDataSetProvider::timerThreadProcedure()
{
    int count = 0;
    while (!stopTimer)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        count++;
        if (count > 9)
        {
            count = 0;
            for (DIMDataSetProvider *it : providers)
            {
                // Manage refresh timeout of value
                if (it->minRefresh_ > -1)
                {
                    ++it->lastRefresh_;
                    // Timeout refresh
                    if (it->lastRefresh_ > it->minRefresh_)
                    {
                        it->timeoutAlVar_->setActive(true);
                    }
                    else
                    {
                        it->timeoutAlVar_->setActive(false);
                    }
                }

                {
                    std::lock_guard<std::mutex> lock(timerLock);
                    for (AlarmsMap::value_type &aIt : it->alarms_)
                    {
                        FIELD_DESC &desc = aIt.second;
                        if (desc.minActiveSince >= 0)
                        {
                            if (desc.minActiveSince >= desc.delay)
                            {
                                if (desc.minAlVar.get() != nullptr)
                                {
                                    desc.minAlVar->setActive(true);
                                }
                            }
                            else
                            {
                                ++desc.minActiveSince;
                            }
                        }
                        if (desc.maxActiveSince >= 0)
                        {
                            if (desc.maxActiveSince >= desc.delay)
                            {
                                if (desc.maxAlVar.get() != nullptr)
                                {
                                    desc.maxAlVar->setActive(true);
                                }
                            }
                            else
                            {
                                ++desc.maxActiveSince;
                            }
                        }
                    }
                }
            }
        }
    }
}

} /* namespace alarm */
} /* namespace ntof */
