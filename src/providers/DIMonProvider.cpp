/*
 * DIMonProvider.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#include "DIMonProvider.h"

#include "DIMonServer.h"

namespace ntof {
namespace alarm {

DIMonProvider::DIMonProvider(pugi::xml_node &providerNode) :
    AlarmProvider(providerNode)
{
    for (pugi::xml_node server = providerNode.first_child(); server;
         server = server.next_sibling())
    {
        // Check if it's a DIM server to be monitored
        if (std::string(server.name()) == "server")
        {
            servers.push_back(ServerPtr(new DIMonServer(*this, server)));
        }
    }
}

DIMonProvider::~DIMonProvider()
{
    // Remove all pending actions in DIMon
    DIMonServer::cleanup();
    // TODO Auto-generated destructor stub
}

} /* namespace alarm */
} /* namespace ntof */
