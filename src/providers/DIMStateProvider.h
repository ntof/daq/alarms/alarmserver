/*
 * DIMStateProvider.h
 *
 *  Created on: Sep 29, 2015
 *      Author: mdonze
 */

#ifndef DIMSTATEPROVIDER_H_
#define DIMSTATEPROVIDER_H_

#include <map>
#include <memory>
#include <set>

#include <AlarmVariable.h>
#include <DIMStateClient.h>
#include <providers/AlarmProvider.h>

namespace ntof {
namespace alarm {

/**
 * Provide alarms for monitoring DIM state service
 */
class DIMStateProvider : public AlarmProvider, public ntof::dim::DIMStateHandler
{
public:
    explicit DIMStateProvider(pugi::xml_node &providerNode);
    ~DIMStateProvider() override;

private:
    /**
     * Called when a DIM state is in error
     * @param errCode
     * @param errMsg
     * @param client
     */
    void errorReceived(int32_t errCode,
                       const std::string &errMsg,
                       const ntof::dim::DIMStateClient *client) override;

    /**
     * Called when a DIM state is refreshed
     * @param errCode
     * @param errMsg
     * @param client
     */
    void stateReceived(int32_t stateValue,
                       const std::string &stateString,
                       const ntof::dim::DIMStateClient *client) override;

    /**
     * No link on DIM state service
     * @param client
     */
    void noLink(const ntof::dim::DIMStateClient *client) override;

    void buildAndRefreshAlarms();

    typedef std::map<std::string, AlarmVariable::Shared> AlarmsMap;
    typedef std::set<int32_t> IgnoreCodeSet;
    typedef std::set<std::string> IgnoreNameSet;

    ntof::dim::DIMStateClient *client;
    AlarmVariable::Shared noLinkAlarm_;
    AlarmsMap alarms_;
    IgnoreCodeSet excludedErrorsCode;   //!< Excluded error codes
    IgnoreCodeSet excludedWarningsCode; //!< Excluded warning codes
    IgnoreNameSet excludedErrorsName;   //!< Excluded error name
    IgnoreNameSet excludedWarningsName; //!< Excluded warning name

    std::string label_;  //!< Alarm label (will be the subsystem)
    std::string system_; //!< Alarm system name
};

} /* namespace alarm */
} /* namespace ntof */

#endif /* DIMStatePROVIDER_H_ */
