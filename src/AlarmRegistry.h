/*
 * AlarmRegistry.h
 *
 *  Created on: Nov 25, 2014
 *      Author: mdonze
 */

#ifndef ALARMREGISTRY_H_
#define ALARMREGISTRY_H_

#include <functional>
#include <thread>
#include <vector>

#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "AlarmVariable.h"

namespace ntof {
namespace alarm {

/**
 * This class will concentrate all alarms into one unique map
 */
class AlarmRegistry :
    private ntof::utils::Thread,
    public ntof::utils::Singleton<AlarmRegistry>
{
public:
    /**
     * @brief Destructor of the class
     */
    ~AlarmRegistry() override;

    AlarmVariable::Shared buildAlarm(const std::string &message,
                                     AlarmVariable::Severity severity,
                                     const std::string &identifier,
                                     const std::string &systemName,
                                     const std::string &subSystem,
                                     const std::string &source,
                                     const std::string &experience);

    /**
     * Gets an alarm pointer from identifier
     * @param identifier Alarm identifier
     * @return Pointer to alarm or NULL
     */
    AlarmVariable::Shared getAlarm(const std::string &identifier) const;

    void postNotification(const AlarmVariable::Shared &notification);
    /**
     * Removes non-exisiting AlarmVariable to list
     */
    void doCleanUp();

    /**
     * Mask an alarm
     * @param ident
     */
    bool maskAlarm(const std::string &ident, std::string &errMsg);

    /**
     * Disable an alarm
     * @param ident Alarm identifier
     */
    bool disableAlarm(const std::string &ident, std::string &errMsg);

    static std::string serialize(const std::vector<AlarmVariable::Shared> &vars);

    /**
     * Removes all alarms
     */
    void clearAllAlarms();

    /**
     * @brief get complete alarms map snapshot
     */
    AlarmVariable::AlarmMap getAllAlarms() const;

    /**
     * @brief test if alarm is known
     */
    bool isKnown(const std::string &identifier) const;

    /**
     * @brief iterate on active alarms
     * @details AlarmRegistry is locked during iteration
     */
    void forEachActive(std::function<void(const AlarmVariable &)>) const;

protected:
    friend class ntof::utils::Singleton<AlarmRegistry>;

    AlarmRegistry();

    /**
     * @brief Start the thread
     */
    void thread_func() override;

    mutable std::mutex m_alarmsLock;
    AlarmVariable::AlarmMap m_alarms; //!<< Map containing active alarms
    mutable std::mutex m_activeAlarmsLock;
    AlarmVariable::AlarmMap m_activeAlarms; //!<< Map containing active alarms
    ntof::utils::Queue<AlarmVariable::Shared> m_queue; //!<< Queue for
                                                       //!< inter-process
                                                       //!< communication
    void buildAndPostXML(); //!<< Build XML and post it to DIM and WebSocket
};

} /* namespace alarm */
} // namespace ntof

#endif /* ALARMREGISTRY_H_ */
