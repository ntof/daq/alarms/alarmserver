/*
 * AlarmPersistency.h
 *
 *  Created on: Nov 25, 2014
 *      Author: mdonze
 */

#ifndef ALARMPERSISTENCY_H_
#define ALARMPERSISTENCY_H_

#include <atomic>
#include <list>
#include <string>
#include <unordered_map>

#include <Singleton.hpp>
#include <Thread.hpp>

#include "AlarmVariable.h"

namespace ntof {
namespace alarm {

typedef std::unordered_map<std::string, AlarmVariable> AlarmVariableMap;

class AlarmPersistency :
    private ntof::utils::Thread,
    public ntof::utils::Singleton<AlarmPersistency>
{
public:
    /**
     * @brief Destructor of the class
     */
    ~AlarmPersistency() override = default;

    /**
     * Sets reference to variable
     * @param ref
     */
    void setVariablesReference(AlarmVariable::AlarmMap *ref);

    /**
     * Signal that some data changed
     */
    void setDataChanged();

    /**
     * Restores variable state from persistency file
     * @param var
     */
    void restoreVariableState(AlarmVariable *var);

protected:
    friend class ntof::utils::Singleton<AlarmPersistency>;

    AlarmPersistency();

    /**
     * @brief Start the thread
     */
    void thread_func() override;

    void loadPersistency();

    AlarmVariableMap m_loadedVars; //!<< Map containing loaded alarms
    std::atomic<bool> m_dataChanged;
};

} /* namespace alarm */
} // namespace ntof

#endif /* ALARMPERSISTENCY_H_ */
