/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-01T11:16:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
**
*/

#include "Config.hpp"

#include <string>

#include <NTOFException.h>
#include <NTOFLogging.hpp>
#include <pugixml.hpp>

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::alarm::Config>;

namespace ntof {
namespace alarm {

static const std::string s_empty;
static const std::map<ProviderType, std::string> s_providerTypeMap{
    {DIMON, "DIMON"},
    {DIMSTATE, "DIMSTATE"},
    {DIMDATASET, "DIMDATASET"},
    {UNKNOWN, "UNKNOWN"},
};

const std::string &toString(ProviderType providerType)
{
    std::map<ProviderType, std::string>::const_iterator it =
        s_providerTypeMap.find(providerType);

    return (it != s_providerTypeMap.end()) ? it->second : s_empty;
}

ProviderType fromString(const std::string &providerType)
{
    ProviderType bt = ProviderType::UNKNOWN;
    if (providerType == "DIMON")
    {
        bt = ProviderType::DIMON;
    }
    else if (providerType == "DIMSTATE")
    {
        bt = ProviderType::DIMSTATE;
    }
    else if (providerType == "DIMDATASET")
    {
        bt = ProviderType::DIMDATASET;
    }
    return bt;
}

const std::string Config::configFile = "/etc/ntof/alarmServer.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile)
{
    LOG(INFO) << "Loading configuration...";
    pugi::xml_document doc;
    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        throw NTOFException("Unable to read the configuration file : " + file,
                            __FILE__, __LINE__);
    }

    pugi::xml_node rootNode = doc.first_child();
    pugi::xml_node configNode = rootNode.child("configuration");

    pugi::xml_node zoneNode = configNode.child("zone");
    if (!zoneNode)
    {
        throw NTOFException("Missing <zone> under configuration : " + file,
                            __FILE__, __LINE__);
    }
    m_experiment = zoneNode.attribute("name").as_string();

    pugi::xml_node serverNode = configNode.child("serverName");
    if (!serverNode)
    {
        m_dimServerName = "ntof-alarms";
    }
    else
    {
        m_dimServerName = serverNode.attribute("value").as_string(
            "ntof-alarms");
    }

    pugi::xml_node serviceNode = configNode.child("serviceName");
    if (!serviceNode)
    {
        m_serviceName = "Alarms";
    }
    else
    {
        m_serviceName = serviceNode.attribute("value").as_string("Alarms");
    }

    pugi::xml_node persistencyNode = configNode.child("persistency");
    if (!persistencyNode)
    {
        m_persistencyFile = "/root/Alarms";
        m_persistencyEnabled = true;
    }
    else
    {
        m_persistencyFile = persistencyNode.attribute("file").as_string(
            "/root/Alarms");
        m_persistencyEnabled = persistencyNode.attribute("enabled").as_bool(
            true);
    }

    pugi::xml_node providersNode = rootNode.child("providers");
    if (providersNode)
    {
        m_providersDoc.reset(new pugi::xml_document());
        m_providersDoc->append_copy(providersNode);
    }
    else
    {
        throw NTOFException("Missing providers in configuration: " + file,
                            __FILE__, __LINE__);
    }
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getExperiment() const
{
    return m_experiment;
}

const std::string &Config::getDIMServerName() const
{
    return m_dimServerName;
}

const std::string &Config::getServiceName() const
{
    return m_serviceName;
}

const std::string &Config::getPersistencyFilePath() const
{
    return m_persistencyFile;
}

bool Config::isPersistencyEnabled() const
{
    return m_persistencyEnabled;
}

std::shared_ptr<pugi::xml_document> Config::getProvidersDoc() const
{
    return m_providersDoc;
}

} // namespace alarm
} /* namespace ntof */
