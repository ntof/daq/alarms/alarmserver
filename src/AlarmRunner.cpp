
#include "AlarmRunner.h"

#include <chrono>
#include <iostream>
#include <thread>
#include <utility>

#include <NTOFLogging.hpp>
#include <pugixml.hpp>

#include "AlarmProviderFactory.h"
#include "AlarmRegistry.h"
#include "AlarmServer.h"
#include "AlarmVariable.h"
#include "Config.hpp"

bool signalReceived = false;

void signalHandler(int signum)
{
    LOG(WARNING) << "Interrupt signal (" << signum << ") received.\n";
    signalReceived = true;
}

namespace ntof {
namespace alarm {

AlarmRunner::AlarmRunner(std::string iConf, std::string iMiscConf) :
    m_configFile(std::move(iConf)), m_configMiscFile(std::move(iMiscConf))
{}

[[noreturn]] void ntof::alarm::AlarmRunner::runner()
{
    // Install signal handler
    signal(SIGHUP, signalHandler);

    // Start Singletons/Threads. thread starts automatically
    server::AlarmServer::instance();
    AlarmRegistry::instance();

    // Vector containing all providers
    std::vector<ntof::alarm::ProviderPtr> providers;

    while (true)
    {
        Config::load(m_configFile, m_configMiscFile);

        const std::shared_ptr<pugi::xml_document> &providersDoc =
            Config::instance().getProvidersDoc();
        pugi::xml_node providersNode = providersDoc->first_child();
        for (pugi::xml_node provNode = providersNode.first_child(); provNode;
             provNode = provNode.next_sibling())
        {
            ntof::alarm::ProviderPtr prov =
                ntof::alarm::AlarmProviderFactory::buildProvider(provNode);
            if (prov)
            {
                providers.push_back(prov);
                LOG(INFO) << "Provider " << prov->getName() << " created.";
            }
            else
            {
                LOG(WARNING) << "Unknown provider";
            }
        }
        AlarmRegistry::instance().doCleanUp();

        while (true)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            if (signalReceived)
            {
                LOG(INFO) << "Reloading alarms...";
                signalReceived = false;
                providers.clear();
                AlarmRegistry::instance().clearAllAlarms();
                break;
            }
        }
    }
}

} // namespace alarm
} // namespace ntof
