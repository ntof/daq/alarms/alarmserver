/*
 * AlarmPersistency.cpp
 *
 *  Created on: Nov 25, 2014
 *      Author: mdonze
 */

#include "AlarmPersistency.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <NTOFLogging.hpp>

#include "AlarmRegistry.h"
#include "AlarmVariable.h"
#include "Config.hpp"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::alarm::AlarmPersistency>;

namespace ntof {
namespace alarm {

AlarmPersistency::AlarmPersistency() :
    Thread("AlarmPersistency"), m_dataChanged(false)
{
    if (Config::instance().isPersistencyEnabled())
    {
        loadPersistency();
        start();
    }
}

/**
 * Restores variable state from persistency file
 * @param var
 */
void AlarmPersistency::restoreVariableState(AlarmVariable *var)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    auto it = m_loadedVars.find(var->getIdentifier());
    if (it != m_loadedVars.end())
    {
        LOG(INFO)
            << "Restoring alarm state for " << var->identifier_ << " found!";
        // Var found. Restores alarm state
        var->active_ = it->second.active_;
        var->lastActive = it->second.lastActive;
        var->masked_ = it->second.masked_;
        LOG(INFO) << "Alarm was masked ? " << (var->masked_ ? "True" : "False");
        var->disabled_ = it->second.disabled_;
    }
}

/**
 * Signal that some data changed
 */
void AlarmPersistency::setDataChanged()
{
    m_dataChanged.store(true);
}

/**
 * Load persistency file
 */
void AlarmPersistency::loadPersistency()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    LOG(INFO) << "Loading persistency file";
    std::ifstream ifS;
    ifS.open(Config::instance().getPersistencyFilePath());

    std::string line;
    while (std::getline(ifS, line))
    {
        std::istringstream stream(line);
        ntof::alarm::AlarmVariable v;
        stream >> v;

        if (v.isValid())
        {
            LOG(INFO)
                << "Adding variable " << v.getIdentifier() << " to persistency";
            m_loadedVars[v.getIdentifier()] = std::move(v);
        }

        // extract the rest using the stream buffer overload
        stream >> std::cout.rdbuf();
    }
    ifS.close();
}

/**
 * Thread to save persistency
 */
void AlarmPersistency::thread_func()
{
    LOG(INFO) << "Persistency thread started";
    while (m_started)
    {
        std::this_thread::sleep_for(std::chrono::seconds(60));
        if (m_dataChanged.exchange(false))
        {
            std::ofstream of;
            of.open(Config::instance().getPersistencyFilePath());
            AlarmRegistry::instance().forEachActive(
                [&of](const AlarmVariable &alarm) { of << alarm; });
            LOG(INFO) << "Writing persistency file...";
            of.close();
        }
    }
}

} /* namespace alarm */
} // namespace ntof
