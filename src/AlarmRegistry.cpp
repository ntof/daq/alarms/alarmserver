/*
 * AlarmRegistry.cpp
 *
 *  Created on: Nov 25, 2014
 *      Author: mdonze
 */

#include "AlarmRegistry.h"

#include <algorithm>
#include <iostream>
#include <vector>

#include <NTOFException.h>
#include <NTOFLogging.hpp>
#include <pugixml.hpp>

#include "AlarmPersistency.h"
#include "AlarmServer.h"
#include "AlarmVariable.h"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::alarm::AlarmRegistry>;

namespace ntof {
namespace alarm {

AlarmRegistry::AlarmRegistry() :
    Thread("AlarmRegistry"), m_queue("Notification", 2048)
{
    LOG(INFO) << "Starting AlarmRegistry...";
    start();
}

AlarmRegistry::~AlarmRegistry()
{
    m_queue.clear(true);
    stop();
}

void AlarmRegistry::thread_func()
{
    std::vector<AlarmVariable::Shared> notifications;
    notifications.reserve(250);

    while (m_started)
    {
        notifications.clear();
        try
        {
            AlarmVariable::Shared alarmChanged = m_queue.pop();
            notifications.push_back(alarmChanged);

            for (int i = 0; i < 50; i++)
            {
                // Try to get other notifications within a time of 200ms
                notifications.push_back(m_queue.pop(5));
            }
        }
        catch (const ntof::NTOFException &ex)
        { /*Timeout in queue, it's time to process data*/
            LOG(WARNING) << "[AlarmRegistry]: " << ex.getMessage();
        }

        // Get list of active alarms
        for (AlarmVariable::Shared &it : notifications)
        {
            if (!it)
            {
                LOG(WARNING) << "[AlarmRegistry]: invalid Alarm notification";
                continue;
            }

            // Get the lock to protect m_activeAlarms
            std::lock_guard<std::mutex> lock(m_activeAlarmsLock);
            // Find if vars already exists
            AlarmVariable::AlarmMap::iterator vIt = m_activeAlarms.find(
                it->getIdentifier());
            if (vIt == m_activeAlarms.end())
            {
                // No existing in current variable list, add it
                if (it->isActive())
                {
                    m_activeAlarms[it->getIdentifier()] = it;
                    LOG(INFO) << "<< " << it->getIdentifier();
                }
            }
            else
            {
                if (!it->isActive())
                {
                    // Variable not active anymore
                    LOG(INFO)
                        << ">> " << vIt->second->getIdentifier() << std::endl;
                    m_activeAlarms.erase(vIt);
                }
            }
        }

        // Signal persistency that he can write a file
        AlarmPersistency::instance().setDataChanged();

        // Post active alarms to our clients
        buildAndPostXML();
    }
}

AlarmVariable::Shared AlarmRegistry::buildAlarm(const std::string &message,
                                                AlarmVariable::Severity severity,
                                                const std::string &identifier,
                                                const std::string &systemName,
                                                const std::string &subSystem,
                                                const std::string &source,
                                                const std::string &experience)
{
    std::string newIdent = AlarmVariable::escapeIdent(identifier);

    std::lock_guard<std::mutex> lock(m_alarmsLock);
    AlarmVariable::AlarmMap::iterator it = m_alarms.find(newIdent);
    if (it == m_alarms.end())
    {
        AlarmVariable::Shared ret(new AlarmVariable(message, severity, newIdent,
                                                    systemName, subSystem,
                                                    source, experience));
        ret->m_weak = ret;
        m_alarms[ret->getIdentifier()] = ret;
        return ret;
    }
    else
    {
        return it->second;
    }
}

/**
 * Gets an alarm pointer from identifier
 * @param identifier Alarm identifier
 * @return Pointer to alarm or NULL
 */
AlarmVariable::Shared AlarmRegistry::getAlarm(const std::string &identifier) const
{
    std::lock_guard<std::mutex> lock(m_alarmsLock);
    AlarmVariable::AlarmMap::const_iterator it = m_alarms.find(identifier);
    return (it != m_alarms.end()) ? it->second : AlarmVariable::Shared();
}

void AlarmRegistry::postNotification(const AlarmVariable::Shared &notification)
{
    try
    {
        m_queue.post(notification);
    }
    catch (const ntof::NTOFException &e)
    {
        LOG(ERROR) << "Notification queue is full. Notification missed..."
                   << std::endl;
    }
}

/**
 * Mask an alarm
 * @param ident
 */
bool AlarmRegistry::maskAlarm(const std::string &ident, std::string &errMsg)
{
    AlarmVariable::Shared var = getAlarm(ident);
    if (var.get())
    {
        LOG(INFO) << "Masking alarm [" << ident
                  << "]. New mask value is: " << !var->isMasked();
        var->setMasked(!var->isMasked());
        // Signal persistence that he can write a file
        AlarmPersistency::instance().setDataChanged();

        // Post active alarms to our clients
        buildAndPostXML();
        return true;
    }
    // Not found
    errMsg = "Identifier not found!";
    return false;
}

/**
 * Disable an alarm
 * @param ident Alarm identifier
 */
bool AlarmRegistry::disableAlarm(const std::string &ident, std::string &errMsg)
{
    AlarmVariable::Shared var = getAlarm(ident);
    if (var.get())
    {
        LOG(INFO) << "Disabling alarm [" << ident
                  << "]. New disable value is: " << !var->isDisabled();
        var->setDisabled(!var->isDisabled());
        // Signal persistence that he can write a file
        AlarmPersistency::instance().setDataChanged();

        // Post active alarms to our clients
        buildAndPostXML();
        return true;
    }
    // Not found
    errMsg = "Identifier not found!";
    return false;
}

struct AlarmSorter
{
    bool operator()(const AlarmVariable::Shared &i,
                    const AlarmVariable::Shared &j)
    {
        return i->getWhen() > j->getWhen();
    }
} mySorter;

/**
 * Build XML data and post it to TCP Server
 */
void AlarmRegistry::buildAndPostXML()
{
    std::vector<AlarmVariable::Shared> varsSorted;

    {
        std::lock_guard<std::mutex> lock(m_activeAlarmsLock);
        varsSorted.reserve(m_activeAlarms.size());
        for (const AlarmVariable::AlarmMap::value_type &it : m_activeAlarms)
        {
            varsSorted.push_back(it.second);
        }
    }

    std::sort(varsSorted.begin(), varsSorted.end(), mySorter);

    std::string msgStr = serialize(varsSorted);
    server::XMLMsg_ptr msg(new server::XMLMessage(msgStr));
    server::AlarmServer::instance().post(msg);
}

std::string AlarmRegistry::serialize(
    const std::vector<AlarmVariable::Shared> &vars)
{
    std::ostringstream oss;

    pugi::xml_document doc;
    pugi::xml_node serverNode = doc.append_child("alarmserver");
    pugi::xml_node parNode = serverNode.append_child("alarms");

    parNode.append_attribute("count").set_value(
        (unsigned long long int) vars.size());

    // Initialize stats component
    std::map<std::string, uint32_t> stats;
    for (auto sev : AlarmVariable::AllSeverity)
    {
        std::string sevStr = AlarmVariable::severityToString(sev);
        stats.insert(std::pair<std::string, uint32_t>(sevStr.c_str(), 0));
    }
    stats.insert(std::pair<std::string, uint32_t>("MASKED", 0));
    stats.insert(std::pair<std::string, uint32_t>("DISABLED", 0));

    for (const auto &var : vars)
    {
        pugi::xml_node alNode = parNode.append_child("alarm");
        alNode.append_attribute("ident").set_value(var->getIdentifier().c_str());
        alNode.append_attribute("severity").set_value(var->getSeverity());
        const std::string severityStr = AlarmVariable::severityToString(
            var->getSeverity());
        alNode.append_attribute("severityStr").set_value(severityStr.c_str());
        alNode.append_attribute("date").set_value(
            (long long int) var->getWhen());
        alNode.append_attribute("system").set_value(
            var->getSystemName().c_str());
        alNode.append_attribute("subsystem")
            .set_value(var->getSubSystemName().c_str());
        alNode.append_attribute("source").set_value(
            var->getAlarmSource().c_str());
        alNode.append_attribute("experience")
            .set_value(var->getExperience().c_str());
        alNode.append_attribute("masked").set_value(var->isMasked() ? "true" :
                                                                      "false");
        alNode.append_attribute("disabled")
            .set_value(var->isDisabled() ? "true" : "false");
        alNode.append_attribute("active").set_value(var->isActive() ? "true" :
                                                                      "false");
        alNode.append_child(pugi::node_pcdata)
            .set_value(var->getMessage().c_str());

        // Update stats
        if (!var->isMasked() && !var->isDisabled())
        {
            stats[severityStr]++;
        }
        if (var->isMasked())
        {
            stats["MASKED"]++;
        }
        if (var->isDisabled())
        {
            stats["DISABLED"]++;
        }
    }

    // create stats element
    pugi::xml_node statsNode = serverNode.append_child("stats");
    for (const auto stat : stats)
    {
        std::string key = stat.first;
        uint32_t value = stat.second;
        // key to lower case
        std::transform(key.begin(), key.end(), key.begin(),
                       [](unsigned char c) { return std::tolower(c); });

        statsNode.append_attribute(key.c_str()).set_value(value);
    }
    doc.save(oss);
    return oss.str();
}

/**
 * Removes non-exisiting AlarmVariable to list
 */
void AlarmRegistry::doCleanUp()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    for (AlarmVariable::AlarmMap::iterator it = m_activeAlarms.begin();
         it != m_activeAlarms.end();)
    {
        AlarmVariable::Shared var = it->second;
        if (!isKnown(var->getIdentifier()))
        {
            it = m_activeAlarms.erase(it);
        }
        else
        {
            ++it;
        }
    }
    lock.unlock();
    // Signal persistency that he can write a file
    AlarmPersistency::instance().setDataChanged();

    // Post active alarms to our clients
    buildAndPostXML();
}

/**
 * Removes all alarms
 */
void AlarmRegistry::clearAllAlarms()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_activeAlarms.clear();
    m_queue.clear();
    m_alarms.clear();
}

AlarmVariable::AlarmMap AlarmRegistry::getAllAlarms() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_alarms;
}

bool AlarmRegistry::isKnown(const std::string &identifier) const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_alarms.count(identifier) != 0;
}

void AlarmRegistry::forEachActive(
    std::function<void(const AlarmVariable &)> fun) const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    for (const AlarmVariable::AlarmMap::value_type &alarm : m_activeAlarms)
    {
        fun(*(alarm.second));
    }
}

} /* namespace alarm */
} // namespace ntof
