/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-01T11:16:40+02:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
**
*/

#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <pugixml.hpp>
#include <sys/types.h>

namespace ntof {
namespace alarm {

enum ProviderType
{
    DIMON,
    DIMSTATE,
    DIMDATASET,
    UNKNOWN
};

const std::string &toString(ProviderType providerType);
ProviderType fromString(const std::string &providerType);

/**
 * @class Config
 * @brief Class used to read the config file
 */
class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    static const std::string configFile;     //!< Path of the config file
    static const std::string configMiscFile; //!< Path of the global config file

    /**
     * @brief Destructor of the class
     */
    ~Config() override = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file, const std::string &miscFile);

    /**
     * @brief Get the name of the experiment
     * @return The name of the experiment
     */
    const std::string &getExperiment() const;

    /**
     * @brief Get the name of the server
     * @return The name of the server
     */
    const std::string &getDIMServerName() const;

    /**
     * @brief Get the name of the service
     * @return The name of the service
     */
    const std::string &getServiceName() const;

    /**
     * @brief Get the persistency file path
     * @return The persistency file path
     */
    const std::string &getPersistencyFilePath() const;

    /**
     * @brief know if persistency is enabled
     * @return The persistency enabled bool
     */
    bool isPersistencyEnabled() const;

    /**
     * @brief Get the providers xml doc
     * @return the providers xml doc
     */
    std::shared_ptr<pugi::xml_document> getProvidersDoc() const;

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &miscFile = configMiscFile);

    std::string m_experiment;      //!< Name of the experiment (EAR1, EAR2)
    std::string m_dimServerName;   //!< Name of the server
    std::string m_serviceName;     //!< Name of the tcp service
    std::string m_persistencyFile; //!< Persistency file path
    bool m_persistencyEnabled;     //!< Persistency enabled
    std::shared_ptr<pugi::xml_document> m_providersDoc; //!< providers xml doc.
                                                        //!< It will be
                                                        //!< processed by the
                                                        //!< ProvidersFactory
};

} // namespace alarm
} /* namespace ntof */

#endif /* CONFIG_H_ */
