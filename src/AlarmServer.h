/*
 * TCPServer.h
 *
 *  Created on: Nov 27, 2014
 *      Author: mdonze
 */

#ifndef ALARMSERVER_H_
#define ALARMSERVER_H_

#include <deque>
#include <memory>

#include <DIMSuperCommand.h>
#include <DIMXMLService.h>
#include <Queue.h>
#include <Singleton.hpp>
#include <Thread.hpp>

#include "XMLMessage.h"

namespace ntof {
namespace alarm {
namespace server {

typedef std::shared_ptr<XMLMessage> XMLMsg_ptr;

class AlarmServer :
    private ntof::utils::Thread,
    public ntof::dim::DIMSuperCommand,
    public ntof::utils::Singleton<AlarmServer>
{
public:
    enum CmdErrorCodes : int32_t
    {
        BAD_CMD = 100,
        MASKING_ERROR,
        DISABLING_ERROR
    };

    ~AlarmServer() override;

    void post(const XMLMsg_ptr &msg);

protected:
    friend class ntof::utils::Singleton<AlarmServer>;

    AlarmServer();
    void thread_func() override;

    void commandReceived(ntof::dim::DIMCmd &cmdData) override;

    ntof::dim::DIMXMLService xmlSvc;
    ntof::utils::Queue<XMLMsg_ptr> msgQueue;
};

} // namespace server
} // namespace alarm
} // namespace ntof

#endif /* TCPSERVER_H_ */
