//============================================================================
// Name        : BoostSockets.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "AlarmServer.h"

#include <iostream>
#include <utility>

#include <NTOFException.h>
#include <NTOFLogging.hpp>
#include <Singleton.hpp>

#include "AlarmRegistry.h"
#include "Config.hpp"

#include <Singleton.hxx>

#define NTOF_THROW(msg, code) \
    throw ntof::NTOFException(msg, __FILE__, __LINE__, code)

template class ntof::utils::Singleton<ntof::alarm::server::AlarmServer>;

using namespace std;

namespace ntof {
namespace alarm {
namespace server {

AlarmServer::AlarmServer() :
    Thread("AlarmServer"),
    ntof::dim::DIMSuperCommand(Config::instance().getServiceName()),
    xmlSvc(Config::instance().getServiceName()),
    msgQueue("AlarmServer", 20000)
{
    LOG(INFO) << "Starting AlarmServer...";
    start();
}

AlarmServer::~AlarmServer()
{
    msgQueue.clear(true);
    stop();
}

void AlarmServer::post(const XMLMsg_ptr &msg)
{
    if (m_instance)
    {
        msgQueue.post(msg);
    }
    else
    {
        throw NTOFException("Alarm server is not properly initialized!",
                            __FILE__, __LINE__);
    }
}

void AlarmServer::thread_func()
{
    while (m_started)
    {
        try
        {
            XMLMessage::Shared msg;
            {
                std::lock_guard<std::mutex> lock(m_mutex);
                msg = msgQueue.pop();
            }

            pugi::xml_document doc;
            pugi::xml_parse_result parseResult = doc.load_buffer(
                msg->getBody().c_str(), msg->getBody().length());
            if (parseResult)
            {
                xmlSvc.setData(doc);
            }
        }
        catch (ntof::NTOFException &ex)
        {
            LOG(WARNING) << "[AlarmServer]: " << ex.getMessage();
        }
    }
}

void AlarmServer::commandReceived(dim::DIMCmd &cmdData)
{
    pugi::xml_node parent = cmdData.getData();
    std::string cmd = parent.attribute("name").as_string("");
    std::string ident = parent.attribute("ident").as_string("");

    if (cmd.empty() || ident.empty())
    {
        NTOF_THROW("Invalid command : " + cmd + " with ident" + ident,
                   CmdErrorCodes::BAD_CMD);
    }

    LOG(DEBUG) << "received command [" << cmd << "] with ident [" << ident
               << "] and key " << cmdData.getKey();

    try
    {
        std::string errMsg;
        if (cmd == "mask")
        {
            if (!AlarmRegistry::instance().maskAlarm(ident, errMsg))
            {
                NTOF_THROW(
                    "Error while masking ident : " + ident + ". Msg: " + errMsg,
                    CmdErrorCodes::MASKING_ERROR);
            }
        }
        else if (cmd == "disable")
        {
            if (!AlarmRegistry::instance().disableAlarm(ident, errMsg))
            {
                NTOF_THROW("Error while disabling ident : " + ident +
                               ". Msg: " + errMsg,
                           CmdErrorCodes::DISABLING_ERROR);
            }
        }
        else
        {
            NTOF_THROW("Unsupported command : " + cmd, CmdErrorCodes::BAD_CMD);
        }
        setOk(cmdData.getKey(), "OK");
    }
    catch (ntof::NTOFException &ex)
    {
        LOG(ERROR) << "Command failed: " << ex.what();
        setError(cmdData.getKey(), ex.getErrorCode(), ex.getMessage());
    }
    catch (std::exception &ex)
    {
        LOG(ERROR) << "Command failed: " << ex.what();
        setError(cmdData.getKey(), 1, ex.what());
    }
    catch (...)
    {
        LOG(ERROR) << "Command failed: unknown error";
        setError(cmdData.getKey(), 1, "unknown error");
    }
}

} // namespace server
} // namespace alarm
} // namespace ntof
